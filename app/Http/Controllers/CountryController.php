<?php

namespace App\Http\Controllers;

use App\Models\country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = country::all();
        return response()->view('cms.countries.index', ['countries' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.countries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:40',
        ]);
        if (!$validator->fails()) {
            //TODO :SUCCESS
            $country = new country();
            $country->name = $request->get('name');
            $isSaved = $country->save();
            return response()->json(['message' => $isSaved ? 'counrty created successfully'
                : 'failed to create'], $isSaved ? response::HTTP_CREATED : response::HTTP_BAD_REQUEST);
        } else {
            //TODO: FAILED
            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(country $country)
    {
        return response()->view('cms.countries.edit', ['country' => $country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, country $country)
    {
        $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:40',
        ]);
        if (!$validator->fails()) {
            //TODO SUCCCESS
            $country->name = $request->get('name');
            $isSaved = $country->save();
            return response()->json(
                [
                    'message' => $isSaved ? 'counrty Updated successfully' : 'failed to Update'
                ],
                $isSaved ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST
            );
        } else {

            return response()->json([
                'message' => $validator->getMessageBag()->first()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(country $country)
    {
        $isDeleted = $country->delete();
        return response()->json([
            'icon' => $isDeleted ? 'success' : 'danger',
            'title' => $isDeleted ? 'the country is deleted' : 'the delete is failed'
        ], $isDeleted ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST);
    }
}
