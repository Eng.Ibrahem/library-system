<?php

namespace App\Http\Controllers;

use App\Models\category;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = category::all();
        return response()->view('cms.categories.index', ['categories' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('cms.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $request->validate([
            'name' => 'required|string|min:3|max:20',
            'desc' => 'nullable|string|min:3|max:50',
            'visible' => 'in:on|string'
        ], [
            'name.required' => 'Please , Enter category name !'
        ]);
        $category = new category();
        $category->name = $request->get('name');
        $category->description = $request->get('desc');
        $category->is_visible = $request->has('visible');
        $isSaved = $category->save();
        if ('isSaved') {
            session()->flash('type', 'success');
            session()->flash('message', 'Category saved successfulley');
            return redirect()->back();
            // return redirect()->route('categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(category $category)
    {
        return response()->view('cms.categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, category $category)
    {
        $request->validate([
            'name' => 'required|string|min:3|max:20',
            'desc' => 'nullable|string|min:3|max:50',
            'visible' => 'in:on|string'
        ], [
            'name.required' => 'Please , Enter the new category name !'
        ]);
        $category->name = $request->get('name');
        $category->description = $request->get('desc');
        $category->is_visible = $request->has('visible');
        $isSaved = $category->save();
        if ($isSaved) {
            
            session()->flash('Umessage', "the category $category->id updated successfully");
            
            return redirect()->route('categories.index');
        } else
            return redirect()->back();
            // session()->flash('Umessage', 'danger');
            // session()->flash('type', 'danger');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(category $category)
    {
        $deleted = $category->delete();
        if ($deleted){
            return response()->json(['title'=>'Deleted successfully','icon'=>'success']);
        }
        else
            return response()->json(['title' => 'Deleted Failed', 'icon' => 'danger']);

        // if ($deleted) {
        //     return redirect()->back();
        }
    }

