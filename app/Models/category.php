<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use HasFactory;
    
    protected $appends = ['visiblity_status'];
    public function getVisiblityStatusAttribute()
    {
        return $this->is_visible == 1 ? 'visible' : 'Hidden';
    }
}
