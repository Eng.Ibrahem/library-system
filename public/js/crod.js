

function confirmDestroy(url, id, raference) {
    console.log('country :' + id)
    // alert('Alert Example ')

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            deleteitem(url, id, raference);
        }
    })
};

function deleteitem(url, id, raference) {

    axios.delete(url + '/' + id)
        .then(function (response) {
            // handle success
            console.log(response);
            showMessage(response.data);
            raference.closest('tr').remove();
        })
        .catch(function (error) {
            // handle error
            console.log(error);
            showMessage(error.response.data);

        });
}
function showMessage(data) {
    Swal.fire({
        icon: data.icon,
        title: data.title,
        showConfirmButton: false,
        timer: 1500
    });
}

function store(url, data) {

    axios.post(url, data).then(function (response) {
        // handle success
        console.log(response);
        toastr.success(response.data.message)
        document.getElementById('create-form').reset();
    }).catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message)

        });
}

function update(url, data, redirectRoute) {

    axios.put(url, data).then(function (response) {
        // handle success
        console.log(response);
        if (redirectRoute != undefined) {
            window.location.href = redirectRoute;
        } else {
            toastr.success(response.data.message);
        }
    }).catch(function (error) {
            // handle error
            console.log(error);
            toastr.error(error.response.data.message);
        });
}