@extends('cms.parent')

@section('title','Countries')

@section('page-large-title','Countries')
@section('page-small-title','create')

@section('content')
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Country</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" id="create-form">
            @csrf   
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                      
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name">
                  </div>
                </div>
            </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="button" onclick="createItem()" class="btn btn-primary">Submit</button>
                </div>
              </form>
          </div>
            <!-- /.card -->

        </div>
          <!--/.col (left) -->
  
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('styles')
    
@endsection

@section('scripts')

    <script> 
      function createItem(){ 
          let data = { name:document.getElementById('name').value }
          store('/cms/admin/countries',data);
      }
    </script>
@endsection