@extends('cms.parent')

@section('title','Countries')

@section('page-large-title','Countries')
@section('page-small-title','update')

@section('content')
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">update Country</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST">
            @csrf   
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                      
                    <input type="text" name="name" value="{{$country->name}}"  class="form-control" id="name" placeholder="Enter Name">
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="button" onclick="updateItem({{$country->id}})" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
  
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('styles')
    
@endsection

@section('scripts')
    <script>
      function updateItem(id){
        let data = {
          name : document.getElementById('name').value
        }
        update('/cms/admin/countries/'+id , data , '/cms/admin/countries')
      };
    </script>
@endsection