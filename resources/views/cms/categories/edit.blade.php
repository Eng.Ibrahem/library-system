@extends('cms.parent')

@section('title','Categories')

@section('page-large-title','Categories')
@section('page-small-title','update')

@section('content')
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">update Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{route('categories.update',$category->id)}}">
            @csrf   
            @method('PUT')
                <div class="card-body">

                      @if ($errors->any())
                
                <div class="card-body">

                    <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </div> 
                        
                @endif
                @if (session()->has('Umessage'))

                 <div class="alert alert-success {{-- {{session()->get('type')}}   when we use it for many types like danger --}} alert-dismissible"> 
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> success!</h5>
                   {{session()->get('Umessage')}}        {{-- {{session('Umessage')}} --}}

                </div>
                    
                @endif

                  <div class="form-group">
                    <label for="name">Name</label>
                      
                    <input type="text" name="name" @if (old('name')) value="{{old('name')}}" @else value="{{$category->name}}"  @endif   class="form-control" id="name" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="desc" @if (old('desc')) value="{{old('desc')}}" @else value="{{$category->description}}"  @endif class="form-control" id="description" placeholder="description">
                  </div>
                  
                  <div class="form-group">
                    <div class="custom-control custom-switch">
                      <input type="checkbox" name="visible" @if ($category->is_visible) checked @endif class="custom-control-input" id="visible" >
                      <label class="custom-control-label" for="visible">visible</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
  
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('styles')
    
@endsection

@section('scripts')
    
@endsection