@extends('cms.parent')

@section('title','Categories')

@section('page-large-title','Categories')
@section('page-small-title','create')

@section('content')
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Category</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{route('categories.store')}}">
            @csrf   
                <div class="card-body">

                    @if ($errors->any())
                
                <div class="card-body">

                    <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </div> 
                        
                @endif
                @if (session()->has('message'))

                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> success!</h5>
                   {{session()->get('message')}}        {{-- {{session('message')}} --}}

                </div>
                    
                @endif

                


                  <div class="form-group">
                    <label for="name">Name</label>
                      
                    <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="desc" value="{{old('desc')}}" class="form-control" id="description" placeholder="description">
                  </div>
                  
                  <div class="form-group">
                    <div class="custom-control custom-switch">
                      <input type="checkbox" name="visible"  class="custom-control-input" id="visible" checked>
                      <label class="custom-control-label" for="visible">visible</label>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
  
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('styles')
    
@endsection

@section('scripts')
    
@endsection