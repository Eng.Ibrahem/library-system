@extends('cms.parent')

@section('title','Categories')

@section('page-large-title','Categories')
@section('page-small-title','Index')

@section('content')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session()->has('Umessage'))

                     <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-check"></i> success!</h5>
                   {{session()->get('Umessage')}}        {{-- {{session('message')}} --}}

                    </div>
                      
                  @endif
                <table class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Name</th>
                      <th>Visible</th>
                      <th>Created At</th>
                      <th>Updated At</th>
                      <th>Settings</th>

                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($categories as $category)
                          <tr>
                      <td>{{$category->id}}</td>
                      <td>{{$category->name}}</td>
                      <td>
                          <span class="badge @if($category->is_visible) bg-success @else bg-danger @endif ">{{$category->visiblity_status}}</span>
                          </td>
                      <td>{{$category->created_at->format('y-m-d H:ma')}}</td>
                      <td>{{$category->updated_at->format('y-m-d H:ma')}}</td>
                      <td>

                          <div class="btn-group">
                        <a href="{{route('categories.edit', $category->id)}}" class="btn btn-info">
                          <i class="fas fa-edit"></i>
                        </a>

                        <a onclick="deleteCategory({{$category->id}}, this)" class="btn btn-danger">
                          <i class="fas fa-trash"></i>
                        </a>

                         {{-- cms/admin/categories/{category} --}}
                        {{-- <form method="POST" action="{{route('categories.destroy',$category->id)}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">
                          <i class="fas fa-trash"></i>
                        </button>
                        </form> --}}
                        
                        
                        </div>
                      </td>
                    </tr>
                 
                      @endforeach
                    
                  </tbody>
                </table>
              </div>
              
   
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
         
          <!-- /.col -->
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('styles')
    
@endsection

@section('scripts')

    <script>
      function deleteCategory(id, raference){
        confirmDestroy('/cms/admin/categories',id, raference)
      }
        
    </script>
@endsection

